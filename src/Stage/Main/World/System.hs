{-# OPTIONS -Wwarn=unused-matches #-}
{-# OPTIONS -Wwarn=unused-local-binds #-}

module Stage.Main.World.System where

import RIO.Local

import Control.Monad (replicateM)
import Control.Monad.State.Strict (StateT)
import Control.Monad.Trans.Resource (ResourceT)
import Apecs.STM.Prelude qualified as Apecs
import Engine.Camera qualified as Camera
import Engine.Sound.Source qualified as SoundSource
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Resource.Collection (enumerate)
-- import RIO.Partial (toEnum)
import RIO.Set qualified as Set
import RIO.List qualified as List
import System.Random (newStdGen)
import System.Random.Stateful qualified as Random

import Global.Resource.Assets (Assets(..))
import Global.Resource.Assets qualified as Assets
import Global.Resource.Model qualified as Model
import Global.Resource.Sound qualified as Sound
-- import Stage.Main.UI qualified as UI
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (RunState(..))
import Stage.Main.World qualified as World
import Stage.Main.World.Body qualified as Body
import Stage.Main.World.Racer qualified as Racer
import Stage.Main.World.Racer.Event qualified as RacerEvent
import Stage.Main.World.Spark qualified as Spark
import Stage.Main.World.Type (CanSet, World, StmWorld, initWorld)
import Stage.Main.World.Wire (Wire(..))
import Stage.Main.World.Wire qualified as Wire
import Utils.Trajectory qualified as Trajectory
import Stage.Main.UI qualified as UI

type DT = Float

spawn
  :: Assets
  -> Camera.ProjectionProcess 'Camera.Perspective
  -> Scene.CameraProcess
  -> Scene.Process
  -> UI.Process
  -> ResourceT (Engine.StageRIO env) World.Simulation
spawn assets projectionP cameraP sceneP uiP =
  World.spawnSimulation interval True (initAction assets) $
    simulationStep assets projectionP cameraP sceneP uiP dt
  where
    interval = 10_000
    dt = fromIntegral interval / 1_000_000

initAction :: Assets -> Engine.StageRIO env World
initAction assets = do
  logInfo "Spawning world..."

  world <- liftIO initWorld
  gen0 <- newStdGen
  gen1 <- newStdGen

  SoundSource.play [Sound.warp_in $ Assets.aSounds assets]

  atomically $ Apecs.runWith world do
    Apecs.set Apecs.global $ World.Rand gen1

    void $! spawnRacer
      assets
      0
      0
      (vec3 4 0 0)
      ( Racer.ControlKB
      , Racer.Forward mempty
      , Racer.Backward mempty
      )

    void $! spawnCluster
      5
      0
      ( Left
          ( (0, 1)
          , (10, 20)
          )
      )
      (2, 5)

    sequence_ $
      let
        genBody = \case
          (ix, _gen) | ix < 1 ->
            Nothing
          (ix, stGen) ->
            let
              (cmd, stGenNext) = Random.runSTGen stGen \gen -> do
                x <- Random.uniformRM (50, 4000) gen
                y01 <- Random.uniformFloat01M gen
                let y = (y01 * y01 - 0.5) * 500
                let pos = vec3 x y 0

                gravmass <- Random.uniformRM (10, 100) gen
                let radius = log gravmass

                turnX <- Random.uniformFloat01M gen
                turnY <- Random.uniformFloat01M gen
                turnZ <- Random.uniformFloat01M gen

                let
                  turns = mconcat
                    [ Transform.rotateX $ τ * turnX
                    , Transform.rotateY $ τ * turnY
                    , Transform.rotateZ $ τ * turnZ
                    ]

                pure $ spawnBody
                  assets
                  (pos + World.offset)
                  radius
                  gravmass
                  turns
            in
              Just
                ( cmd
                , (ix - 1, stGenNext)
                )
      in
        List.unfoldr genBody (World.numBodies, gen0)

    goal <- spawnBody
      assets
      World.goalPos
      World.goalRadius
      World.goalGravmass
      (Transform.scale (-1))

    _goalCore <- spawnBody
      assets
      World.goalPos
      10
      10
      mempty

    Apecs.set goal Body.Goal

    Apecs.set Apecs.global $
      World.Time 0

  -- lift $
  --   postMessage_ (uiMessageLog uiProbe) "Hi there."

  pure world

withRandom
  :: (Random.StateGenM Random.StdGen -> StateT Random.StdGen StmWorld a)
  -> StmWorld a
withRandom action = do
  World.Rand genIn <- Apecs.get Apecs.global
  let (genOut, genAction) = Random.split genIn
  Apecs.set Apecs.global $ World.Rand genOut

  Random.runStateGenT_ genAction action

spawnBody
  :: Assets
  -> Vec3
  -> Float
  -> Float
  -> Transform
  -> StmWorld Apecs.Entity
spawnBody assets pos radius gm transform =
  Apecs.newEntity
    ( body
    )
  where
    body = Body.Body
      { model =
          Model.icosphere4 (Assets.aModels assets)
      , position =
          pos
      , radius =
          radius
      , gravmass =
          gm
      , inst =
          transform <>
          Transform.scale radius <>
          Transform.translateV pos
      }

spawnRacer
  :: CanSet extras
  => Assets
  -> Vec3
  -> Float
  -> Vec3
  -> extras
  -> StmWorld Apecs.Entity
spawnRacer assets pos dir vel extras = Apecs.newEntity (racer, extras)
  where
    racer = Racer.Racer
      { model =
          view _3 $ Model.craftRacer (Assets.aModels assets)
      , position =
          pos
      , direction =
          dir
      , velocity =
          vel
      , boostCharge =
          0.5
      , shedCharge =
          0.5
      , jumpCharge =
          0.5
      }

spawnCluster
  :: Int
  -> Vec3
  -> Either ((Float, Float), (Float, Float)) (Vec3, Vec3)
  -> (Float, Float)
  -> StmWorld Apecs.Entity
spawnCluster initialParticles position velocityGen timeRange = do
  particles <- withRandom \gen ->
    replicateM initialParticles do
      velocity <- case velocityGen of
        Right (a, b) -> do
          alpha <- Random.uniformFloat01M gen
          pure $ Vec3.lerp alpha a b
        Left (dirRange, velRange) -> do
          turn <- Random.uniformRM dirRange gen
          let dir = τ * turn
          vel <- Random.uniformRM velRange gen
          pure $ vec3
            (cos dir * vel)
            (sin dir * vel)
            0

      timeMax <- Random.uniformRM timeRange gen
      pure Spark.Particle
        { position =
            position
        , velocity =
            velocity
        , colorsHead =
            ( vec4 1 1 1 1
            , vec4 1 1 1 1
            )
        , colorsTail =
            ( vec4 0 1 1 1
            , vec4 0 1 1 1
            )
        , time =
            0.0
        , timeMax =
            timeMax
        }

  Apecs.newEntity
    ( Spark.Cluster particles
    , Wire mempty
    )

simulationStep
  :: Assets
  -> Camera.ProjectionProcess 'Camera.Perspective
  -> Scene.CameraProcess
  -> Scene.Process
  -> UI.Process
  -> DT
  -> World
  -> Engine.StageRIO env ()
simulationStep assets projectionP cameraP sceneP uiP dt world = do
  -- logDebug "sim step"
  -- gen <- newStdGen
  (plays, stops) <- atomically do
    soundStart <- newTVar (mempty :: Set Int)
    soundStop <- newTVar (mempty :: Set Int)
    let
      ($#) sample active =
        if active then
          modifyTVar soundStart $ Set.insert (sample Sound.indices)
        else
          modifyTVar soundStop $ Set.insert (sample Sound.indices)

    Apecs.runWith world do
      Apecs.modify Apecs.global (+ World.Time dt)

      bodies <- World.collect id

      Apecs.cmap $ applyTurn dt
      Apecs.cmapM $ applyBurn dt
      Apecs.cmapM $ applyShed dt
      Apecs.cmapM $ applyJump dt

      Apecs.cmap $ spendBoost dt
      Apecs.cmap $ spendShed dt
      Apecs.cmap $ spendJump dt

      Apecs.cmap $ projectForward dt bodies
      Apecs.cmapM $ applyForward ($#) bodies

      -- Apecs.cmapM $ applyGravity dt bodies
      -- Apecs.cmapM $ applyVelocity dt

      Apecs.cmapM_ $ trackPlayer projectionP cameraP sceneP

      Apecs.cmapM_ $ updateUI dt ($#) uiP

      Apecs.cmap $ sparkCluster dt

    started <- readTVar soundStart
    stopped <- readTVar soundStop
    pure (Set.difference started stopped, stopped)

  let sources = toList $ enumerate (Assets.aSounds assets)
  unless (Set.null plays) $
    SoundSource.play do
      (ix, source) <- sources
      guard $ Set.member ix plays
      pure source
  unless (Set.null stops) $
    SoundSource.stop do
      (ix, source) <- sources
      guard $ Set.member ix stops
      pure source

type PlaySound = (Sound.Collection Int -> Int) -> Bool -> STM ()

applyTurn
  :: DT
  -> ( Racer.Turn
     , Racer.Racer
     , Racer.NotJumping
     )
  -> Racer.Racer
applyTurn dt (Racer.Turn turn, racer, _notJumping) =
  racer
    { Racer.direction =
        Racer.direction racer + turn * dt
    }

applyBurn
  :: DT
  -> ( Racer.Burn
     , Racer.Racer
     , Maybe Racer.Boost
     , Racer.NotJumping
     )
  -> StmWorld Racer.Racer
applyBurn dt (Racer.Burn burn, racer, mboost, _notJumping) = do
  withRandom \gen -> do
    roll <- Random.uniformRM (0, maxRoll) gen
    when (maxRoll - roll * boostAlpha >= 1.0) do
      offsetDir <- Random.uniformRM (direction - 0.25, direction + 0.25) gen
      let
        offset =
          vec3
          (cos offsetDir)
          (sin offsetDir)
          0
      void . lift $! spawnCluster
        1
        (Racer.position racer - offset)
        ( Right
            ( velocity ^* 0.98
            , velocity ^* 0.99
            )
        )
        (0, 1.0)
  pure racer
    { Racer.velocity =
        velocity +
        vec3
          (cos direction * (burn + boostAlpha * 16.0) * dt)
          (sin direction * (burn + boostAlpha * 16.0) * dt)
          0
    }
  where
    maxRoll = maybe 1 (const 8) mboost

    Racer.Racer{velocity, direction} = racer
    boostAlpha = case mboost of
      Nothing ->
        0
      Just (Racer.Boost timer) ->
        timer / Racer.maxBoostTimer

applyShed
  :: DT
  -> (Racer.Racer, Racer.Shed, Maybe Racer.Forward)
  -> StmWorld (Racer.Racer, Either () Racer.Forward)
applyShed _dt (racer@Racer.Racer{position, velocity}, _shed, mforward) = do
  let dropped = round (sqrt (Vec3.dot velocity velocity) * 0.05)
  when (dropped > 1) do
    void $! spawnCluster
      dropped
      position
      ( Right
          ( Transform.apply velocity $ Transform.rotateZ (-0.5)
          , Transform.apply velocity $ Transform.rotateZ 0.5
          )
      )
      (0, 1)
  pure
    ( racer
       { Racer.velocity =
          velocity * 0.95
       }
    , case mforward of
        Nothing ->
          Left ()
        Just _forward ->
          Right $ Racer.Forward []
    )

applyJump
  :: DT
  -> (Racer.Racer, Racer.Jump, Maybe Racer.Forward)
  -> StmWorld (Racer.Racer, Either () Racer.Forward)
applyJump dt (racer@Racer.Racer{position, direction}, _jump, mforward) = do
  void $! spawnCluster
    5
    (Racer.position racer + jumpVelocity ^* (0.25 / dt))
    ( Left
        ( (0, 1)
        , (5, 10)
        )
    )
    (0, 1) -- TODO: lerp with timer
  pure
    ( racer
       { Racer.position =
          position + jumpVelocity

       }
    , case mforward of
        Nothing ->
          Left ()
        Just _forward ->
          Right $ Racer.Forward []
    )
  where
    jumpSpeed = 10.0

    jumpVelocity = vec3
      (cos direction * jumpSpeed * dt)
      (sin direction * jumpSpeed * dt)
      0

spendBoost
  :: DT
  -> (Racer.Racer, Racer.NotCrashed, Maybe Racer.Boost)
  -> (Either () Racer.Racer, Maybe Racer.Boost)
spendBoost dt (racer, _notCrashed, mboost) =
  case mboost of
    Just (Racer.Boost timeLeft) ->
      let
        nextTime = timeLeft - dt
      in
        ( Left ()
        , if nextTime <= 0 then
            Nothing
          else
            Just $ Racer.Boost nextTime
        )
    _charging ->
      ( Right racer
          { Racer.boostCharge =
              min 3 $ Racer.boostCharge racer + dt / 15.0
          }
      , mboost
      )

spendShed
  :: DT
  -> (Racer.Racer, Racer.NotCrashed, Maybe Racer.Shed)
  -> (Either () Racer.Racer, Maybe Racer.Shed)
spendShed dt (racer, _notCrashed, mshed) =
  case mshed of
    Just (Racer.Shed timeLeft) ->
      let
        nextTime = timeLeft - dt
      in
        ( Left ()
        , if nextTime <= 0 then
            Nothing
          else
            Just $ Racer.Shed nextTime
        )
    _charging ->
      ( Right racer
          { Racer.shedCharge =
              min 2 $ Racer.shedCharge racer + dt / 30.0
          }
      , mshed
      )

spendJump
  :: DT
  -> (Racer.Racer, Racer.NotCrashed, Maybe Racer.Jump)
  -> (Either () Racer.Racer, Maybe Racer.Jump)
spendJump dt (racer, _notCrashed, mjump) =
  case mjump of
    Just (Racer.Jump timeLeft) ->
      let
        nextTime = timeLeft - dt
      in
        ( Left ()
        , if nextTime <= 0 then
            Nothing
          else
            Just $ Racer.Jump nextTime
        )
    _charging ->
      ( Right racer
          { Racer.jumpCharge =
              min 2 $ Racer.jumpCharge racer + dt / 45.0
          }
      , mjump
      )

projectForward
  :: DT
  -> [Body.Body]
  -> (Racer.Racer, Racer.Forward, Maybe Racer.Burn, Racer.NotCrashed, Racer.NotJumping)
  -> Racer.Forward
projectForward dt bodies (racer, Racer.Forward segments, mburn, _notCrashed, _notJumping) =
  case (segments, mburn) of
    (_seg : next, Nothing) ->
      Racer.Forward next
    _notInFreeFall ->
      Racer.Forward updated
  where
    Racer.Racer{position, velocity} = racer
    bodies' = map (Body.gravmass &&& Body.position) bodies

    updated = Trajectory.project dt bodies' (position, velocity)

applyForward
  :: PlaySound
  -> [Body.Body]
  -> (Racer.Racer, Racer.Forward, Racer.Backward, Racer.NotCrashed, Maybe Racer.Jump)
  -> StmWorld (Racer.Racer, Racer.Backward, Wire, Either Racer.Crashed ())
applyForward ($#) bodies components = do
  when isCrashed . lift $
    if isArrived then
      Sound.warp_in $# True
    else
      Sound.crash $# True

  pure case forward of
    _ | isCrashed ->
      ( racer
          { Racer.velocity = 0
          }
      , Racer.Backward $ drop 1 backward
      , mempty
      , Left Racer.Crashed
      )

    [] ->
      ( racer
      , Racer.Backward backward
      , convertBackward $ oldPosition : take 120 backward
      , Right ()
      )

    current : _next ->
      ( racer
          { Racer.position =
              Trajectory.sPos current
          , Racer.velocity =
              Trajectory.sVel current
          }
      , Racer.Backward $
          Trajectory.sPos current : take 120 backward
      , case mJumping of
          Just _jumpTimer ->
            convertBackward backward
          Nothing ->
            foldr
              towardBody
              (convertForward forward <> convertBackward backward)
              bodies
      , Right ()
      )
  where
    (racer, Racer.Forward forward, Racer.Backward backward, _notCrashed, mJumping) = components
    oldPosition = Racer.position racer

    isCrashed =
      case forward of
        Trajectory.Segment{sAnn=Trajectory.Terminal} : _rest ->
          True
        _ ->
          False

    isArrived =
      distanceA (Racer.position racer) World.goalPos < World.goalRadius

    towardBody body acc =
      if tooFar then
        acc
      else
        Wire.consEdge edge acc
      where
        tooFar = distance >= maxDistance

        distance = sqrt qd0

        edge =
          Wire.mkEdge
            (markerStart, color)
            (markerEnd, vec4 0 0 0 0)

        minDistance = 100
        midDistance = 200
        maxDistance = 500

        (markerLen, color) =
          if distance < minDistance then
            ( distance * 0.66
            , Vec4.lerp (distance / minDistance) (vec4 0 1 0 1) (vec4 1 0 0 1)
            )
          else
            if distance < midDistance then
              ( lerpRange (minDistance, distance, maxDistance) 33.3 10.0
              , 0.5
              )
            else
              ( 10.0
              , 0.25
              )

        lerpRange (rMin, x, rMax) a b = a * (1.0 - alpha) + b * alpha
          where
            alpha =
              if x <= rMin then
                0.0
              else
                if x >= rMax then
                  1.0
                else
                  (x - rMin) / (rMax - rMin)

        markerStart =
          racerPos + pos0 ^* (4.0 / distance)

        markerEnd =
          racerPos + pos0 ^* (markerLen / distance)

        racerPos = Racer.position racer
        bodyPos = Body.position body

        pos0 = bodyPos - racerPos
        qd0 = Vec3.dot pos0 pos0

{-
-- | JIT gravity?..
_applyGravity
  :: DT
  -> [Body.Body]
  -> (Racer.Racer, Apecs.Not Racer.Forward)
  -> StmWorld (Racer.Racer, Wire)
_applyGravity dt bodies (racer, _notForward) = do
  pure $
    ( racer
        { Racer.velocity =
            velocity + sum (map fst pulls)
        }
    , foldMap snd pulls
    )
  where
    Racer.Racer{position=racerPosition, velocity} = racer

    pulls = map getPull bodies

    getPull Body.Body{position=bodyPosition, radius, gravmass} =
      if qd > 4096 || qd < 1/8 then
        ( 0
        , velocityWire
        )
      else
        ( direction ^* (dt * pull)
        , velocityWire <> bodyWire
        )
      where
        pull = gravmass / qd
        pullV = bodyPosition - racerPosition
        qd = Vec3.dot pullV pullV
        len = sqrt qd
        direction =
          withVec3 pullV \x y z ->
            vec3 (x / len) (y / len) (z / len)

        velocityWire =
          Wire.mkEdge
            (racerPosition, 1)
            (racerPosition + velocity, 0)

        bodyWire =
          Wire.mkEdge
            (racerPosition, racerColor)
            (bodyPosition, bodyColor)
          where
            racerColor =
              if pull >= 1.0 then
                vec4 1 0 0 1
              else
                vec4 0 1 0 1

            bodyColor =
              if len <= 2*radius then
                vec4 1 0 0 1
              else
                1

_applyVelocity
  :: DT
  -> Racer.Racer
  -> StmWorld Racer.Racer
_applyVelocity dt racer =
  pure $ racer
    { Racer.position =
        position + velocity ^* dt
    }
  where
    Racer.Racer{position, velocity} = racer
-}

convertBackward :: [Vec3] -> Wire
convertBackward path = mconcat do
  (i, (WithVec3 cx cy _cz, WithVec3 prx pry _prz)) <- zip [0.0 ..] segments
  let alpha = (len - i) / len
  let color = vec4 alpha alpha 0 alpha
  pure $
    Wire.mkEdge
      (vec3 cx cy 0, color)
      (vec3 prx pry 0, color)
  where
    segments = zip path (drop 1 path)
    len = fromIntegral (length path)

convertForward :: [Trajectory.Segment] -> Wire
convertForward = \case
  [] ->
    mempty
  segmentsInfinite@(start : _next) ->
    fall (isNothing $ crashAlert segmentsInfinite) start $
      take 240 $
        reducedPath isBoring Trajectory.sPos segmentsInfinite
  where
    fall safe _start path = mconcat do
      (ix, (_seg, a, b)) <- zip [0..] path

      let
        alpha = (len - ix) / len
        color =
          if safe then
            vec4 0 alpha alpha alpha
          else
            vec4 alpha 0 alpha alpha

      pure $ Wire.mkEdge (b, color) (a, color)
      where
        len =
          fromIntegral $ length path

        {-
        unLooped = takeWhile (not . isLooped) path

        isLooped (Trajectory.Segment{..}, _a, _b)
          | sIx - startIx < 10 = False
          | abs (sNorm - startNorm) > startNorm = False
          | otherwise = distanceA sPos startPos < 0.5

        startIx = Trajectory.sIx start
        startPos = Trajectory.sPos start
        startNorm = Trajectory.sNorm start
        -}

    isBoring s =
      case Trajectory.sAnn s of
        Trajectory.Fall | Trajectory.sIx s < 2 ->
          Just False
        Trajectory.Fall ->
          Nothing
        _ ->
          Just False

crashAlert :: [Trajectory.Segment] -> Maybe Float
crashAlert segments = listToMaybe do
  (step, Trajectory.Segment{sAnn=Trajectory.Terminal}) <- zip [0..] $ take 450 segments
  pure step

distanceA :: Vec3 -> Vec3 -> Float
distanceA a b = sqrt $ Vec3.dot ab ab
  where
    ab = a - b

reducedPath
  :: (a -> Maybe Bool)
  -> (a -> Vec3)
  -> [a]
  -> [(a, Vec3, Vec3)]
reducedPath isBoring getPos = \case
  [] ->
    []
  cur : rest ->
    List.unfoldr reduce (getPos cur, rest)
  where
    reduce (curPos, stream) =
      case dropWhile (skipFrom curPos) stream of
        [] ->
          Nothing
        next : rest ->
          let
            nextPos = getPos next
          in
            Just
              ( (next, nextPos, curPos)
              , (nextPos, rest)
              )

    skipFrom curPos next =
      case isBoring next of
        Nothing ->
          let
            d = curPos - getPos next
          in
            Vec3.dot d d < 0.25
        Just boring ->
          boring

trackPlayer
  :: Camera.ProjectionProcess 'Camera.Perspective
  -> Scene.CameraProcess
  -> Scene.Process
  -> (Racer.ControlKB, Racer.Racer)
  -> StmWorld ()
trackPlayer projectionP cameraP sceneP (_tag, racer) =
  lift do
    camera <- readTVar $ Worker.getConfig cameraP

    let
      cameraNext = camera
        { Scene.ccTrack =
            Scene.CameraTrackFixed position
        }
      newView = Camera.mkViewOrbital_ $
        Scene.initialState
          { Camera.orbitTarget = position
          }

    Worker.modifyConfigSTM cameraP $ const cameraNext
    Worker.pushOutputSTM cameraP \_oldView -> newView

    pp <- Worker.getOutputDataSTM projectionP
    Worker.pushOutputSTM sceneP \_old ->
      Scene.mkScene pp newView

  where
    Racer.Racer{position} = racer

updateUI
  :: DT
  -> PlaySound
  -> UI.Process
  -> (Racer.ControlKB, Racer.Racer, Racer.Forward, Maybe Racer.Burn, Maybe Racer.Crashed, ActiveItems)
  -> StmWorld ()
updateUI dt ($#) uiP (Racer.ControlKB, Racer.Racer{..}, Racer.Forward forward, mburn, mcrashed, activeItems) = do
  World.Time globalTime <- Apecs.get Apecs.global
  lift do
    oldCrashAlert <- fmap UI._crashAlert $ Worker.getInputDataSTM uiP
    let newCrashAlert = crashAlert forward

    case (oldCrashAlert, newCrashAlert) of
      (Nothing, Just _) ->
        Sound.crash_alert $# True
      (Just _, Nothing) ->
        Sound.crash_averted $# True
      _ ->
        pure ()

    Worker.updateInputSTM uiP \ui ->
      Just ui
        { UI._playerPosition =
            position
        , UI._playerVelocity =
            velocity

        , UI._crashed =
            isJust mcrashed

        , UI._crashAlert =
            fmap
              (\step -> step * dt * 0.95)
              newCrashAlert

        , UI._goalDistance =
            distanceA position World.goalPos

        , UI._runTime =
            case mcrashed of
              Just _hold ->
                UI._runTime ui
              Nothing ->
                globalTime

        , UI._burnTime =
            if isJust mcrashed || isNothing mburn then
              UI._burnTime ui
            else
              UI._burnTime ui + dt

        , UI._topScore =
            withVec3 position \x _y _z ->
              max (UI._topScore ui) (floor x)
        , UI._topSpeed =
            max (UI._topSpeed ui) $
              sqrt (Vec3.dot velocity velocity)

        , UI._boostItem =
            maybe
              (Right boostCharge)
              (Left . Racer.boostTimer)
              (activeItems ^. _1)

        , UI._shedItem =
            maybe
              (Right shedCharge)
              (Left . Racer.shedTimer)
              (activeItems ^. _2)

        , UI._jumpItem =
            maybe
              (Right jumpCharge)
              (Left . Racer.jumpTimer)
              (activeItems ^. _3)

        , UI._debugDump =
            ""
        }

sparkCluster
  :: DT
  -> (Spark.Cluster, Wire)
  -> (Maybe (Spark.Cluster, Wire))
sparkCluster dt (Spark.Cluster particles, _oldWire) =
  case particles of
    [] ->
      Nothing
    _some ->
      Just (Spark.Cluster newParticles, newWire)
  where
    (newParticles, newWire) = foldr go ([], mempty) particles

    go Spark.Particle{..} skip@(accP, accW) =
      let
        newTime = time + dt
        alpha = 1.0 - newTime / timeMax
        edge = Wire.mkEdge
          ( position
          , uncurry (Vec4.lerp alpha) colorsTail Vec4.^* alpha
          )
          ( position + velocity ^* (dt * 2 * alpha)
          , uncurry (Vec4.lerp alpha) colorsHead Vec4.^* alpha
          )
        newParticle = Spark.Particle
          { position = position + velocity ^* dt
          , velocity = velocity * 0.999
          , time     = newTime
          , ..
          }
      in
        if newTime >= timeMax then
          skip
        else
          ( newParticle : accP
          , Wire.consEdge edge accW
          )

-- * Events

racerEvent :: RacerEvent.Event -> Engine.StageRIO RunState ()
racerEvent = \case
  RacerEvent.Turn eventDir -> do
    world <- gets $ World.sWorld . rsWorldSim
    atomically . Apecs.runWith world $
      Apecs.cmap \(Racer.ControlKB, Racer.Racer{}) ->
        fmap
          ( \direction ->
              Racer.Turn $ direction * 4.0
          )
          eventDir

  RacerEvent.Burn active -> do
    world <- gets $ World.sWorld . rsWorldSim
    atomically . Apecs.runWith world $
      Apecs.cmap \(Racer.ControlKB, Racer.Racer{}) ->
        if active then
          Just $ Racer.Burn 2.0
        else
          Nothing
    gets (Sound.thrust . aSounds . rsAssets) >>=
      SoundSource.toggle active

  RacerEvent.UseBoost -> do
    world <- gets $ World.sWorld . rsWorldSim
    ok <- atomically $ Apecs.runWith world do
      Apecs.cfoldM
        (\_acc ((checks, entity) :: (BoostChecks, Apecs.Entity)) -> do
          let
            racer = checks ^. _2
            boostCharged = Racer.boostCharge racer >= 1.0
          when boostCharged $
            Apecs.set entity
              ( Racer.Boost Racer.maxBoostTimer
              , racer
                  { Racer.boostCharge =
                      Racer.boostCharge racer - 1.0
                  }
              )
          pure boostCharged
        )
        False
    when ok do
      itemBoost <- gets $ Sound.item_boost . aSounds . rsAssets
      SoundSource.play [itemBoost]

  RacerEvent.UseShed -> do
    world <- gets $ World.sWorld . rsWorldSim
    ok <- atomically $ Apecs.runWith world do
      Apecs.cfoldM
        (\_acc ((checks, entity) :: (ShedChecks, Apecs.Entity)) -> do
          let
            racer = checks ^. _2
            shedCharged = Racer.shedCharge racer >= 1.0
          when shedCharged $
            Apecs.set entity
              ( Racer.Shed Racer.maxShedTimer
              , racer
                  { Racer.shedCharge =
                      Racer.shedCharge racer - 1.0
                  }
              )
          pure shedCharged
        )
        False
    when ok do
      itemShed <- gets $ Sound.item_shed . aSounds . rsAssets
      SoundSource.play [itemShed]

  RacerEvent.UseJump -> do
    world <- gets $ World.sWorld . rsWorldSim
    ok <- atomically $ Apecs.runWith world do
      Apecs.cfoldM
        (\_acc ((checks, entity) :: (JumpChecks, Apecs.Entity)) -> do
          let
            racer = checks ^. _2
            jumpCharged = Racer.jumpCharge racer >= 1.0
          when jumpCharged $
            Apecs.set entity
              ( Racer.Jump Racer.maxJumpTimer
              , racer
                  { Racer.jumpCharge =
                      Racer.jumpCharge racer - 1.0
                  }
              )
          pure jumpCharged
        )
        False
    when ok do
      itemJump <- gets $ Sound.item_jump . aSounds . rsAssets
      SoundSource.play [itemJump]

type BoostChecks =
  ( Racer.ControlKB
  , Racer.Racer
  , Racer.NotCrashed
  , Racer.NotBoosted
  )

type ShedChecks =
  ( Racer.ControlKB
  , Racer.Racer
  , Racer.NotCrashed
  , Racer.NotShedding
  )

type JumpChecks =
  ( Racer.ControlKB
  , Racer.Racer
  , Racer.NotCrashed
  , Racer.NotJumping
  )

type ActiveItems =
  ( Maybe Racer.Boost
  , Maybe Racer.Shed
  , Maybe Racer.Jump
  )
