module Stage.Main.Event.Sink
  ( handleEvent
  ) where

import RIO

import RIO.State (gets)

import Engine.StageSwitch (trySwitchStage)
import Engine.Types (StageRIO, StackStage)
import Engine.Types qualified as Engine
-- import Engine.Sound.Source qualified as SoundSource
import Engine.Worker qualified as Worker

-- import Global.Resource.Sound qualified as Sound
import Global.Highscores qualified as Highscores
import Stage.Main.Event.Type (Event(..))
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (RunState(..))
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World
import Stage.Main.World.System qualified as WorldSystem

handleEvent :: StackStage -> Event -> StageRIO RunState ()
handleEvent stackStage = \case
  Pause -> do
    world <- gets rsWorldSim
    atomically $ modifyTVar' (World.sActive world) not

  StepForward -> do
    sim <- gets rsWorldSim
    active <- readTVarIO (World.sActive sim)
    when (not active) do
      assets <- gets rsAssets
      projectionP <- gets rsProjectionP
      cameraP <- gets rsCameraP
      sceneP <- gets rsSceneP
      uiP <- gets rsUIP

      let
        interval = 10_000 :: Int
        dt = fromIntegral interval / 1_000_000
      WorldSystem.simulationStep
        assets
        projectionP
        cameraP
        sceneP
        uiP
        dt
        (World.sWorld sim)
      Worker.updateOutput sim Just

  Camera event -> do
    timer <- gets rsCameraP
    atomically do
      Worker.modifyConfigSTM timer \cc ->
        case event of
          Scene.CameraAzimuth delta ->
            cc { Scene.ccAzimuth = delta }
          Scene.CameraAscent delta ->
            cc { Scene.ccAscent = delta }
          Scene.CameraDistance delta ->
            cc { Scene.ccDistance = delta }
          Scene.CameraPan delta ->
            cc { Scene.ccPan = delta }
      Scene.CameraControls{..} <- readTVar $ Worker.getConfig timer
      writeTVar (Worker.tActive timer) $ or
        [ ccAzimuth  /= 0
        , ccAscent   /= 0
        , ccDistance /= 0
        , ccPan      /= 0.0
        ]

  Racer event ->
    WorldSystem.racerEvent event

  Restart -> do
    timer <- gets rsCameraP
    atomically $ writeTVar (Worker.tActive timer) False

    sim <- gets rsWorldSim
    atomically $ writeTVar (World.sActive sim) False

    ui <- gets rsUIP >>= Worker.getInputData
    Highscores.submit
      (Highscores.playerName $ UI._player ui)
      (ceiling $ UI._topSpeed ui)
      ( if UI._goalDistance ui < World.goalRadius then
          Just $ ceiling $ UI._runTime ui
        else
          Nothing
      )
      ( if UI._goalDistance ui < World.goalRadius then
          Just $ ceiling $ UI._burnTime ui
        else
          Nothing
      )

    void $! trySwitchStage (Engine.Replace stackStage)
