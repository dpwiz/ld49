module Stage.Main.Types where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Types (StageRIO)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render
import Global.Resource.Assets (Assets)
import Stage.Main.Event.Type (Event)
import Stage.Main.Scene qualified as Scene
-- import Stage.Main.UI (UI)
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World
import Utils.Sink (MonadSink(..))

type Stage = Render.Stage FrameResources RunState
type Frame = Render.Frame FrameResources

data FrameResources = FrameResources
  { frScene   :: Set0.FrameResource '[Set0.Scene]
  , frSceneUi :: Set0.FrameResource '[Set0.Scene]

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun

  -- , frUI :: UI.Observer

  , frWorld :: World.Observer
  }

data RunState = RunState
  { rsEvents :: Maybe (Events.Sink Event RunState)

  , rsProjectionP   :: Camera.ProjectionProcess 'Camera.Perspective
  , rsCameraP       :: Scene.CameraProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process

  , rsSceneUiP :: Scene.Process

  , rsUpdateShadowP :: Worker.Var ()
  , rsUpdateShadow  :: Worker.ObserverIO ()

  , rsUIP :: UI.Process
  , rsWorldSim :: World.Simulation

  , rsAssets :: Assets
  }

instance MonadSink Event RunState (StageRIO RunState) where
  getSink = gets rsEvents
  sendSinkSignal = id

instance MonadSink Event RunState (Render.StageFrameRIO FrameResources RunState) where
  getSink = gets rsEvents
  sendSinkSignal = mapRIO fst
