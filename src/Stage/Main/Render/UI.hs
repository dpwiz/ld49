module Stage.Main.Render.UI
  ( imguiDrawData
  ) where

import RIO.Local

-- import DearImGui.Raw qualified as Raw
import DearImGui qualified
import Engine.Worker qualified as Worker
import Foreign.C (CFloat(..))
import Render.ImGui qualified as ImGui
import RIO.Text qualified as Text

import Global.Render (StageFrameRIO)
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.Event.Type as Event
import Stage.Main.UI qualified as UI

import Utils.Dear qualified as Dear

type DrawM = StageFrameRIO FrameResources RunState

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = do
  uiP <- gets rsUIP
  ui <- gets rsUIP >>=
    Worker.getOutputData

  fmap snd $ ImGui.mkDrawData do
    Dear.window_ (UI.status ui) do
      DearImGui.text (UI.goalDistance ui)
      DearImGui.textColored
        (pure @IO $ DearImGui.ImVec4 1 0 0 1)
        (UI.crashAlert ui)

      DearImGui.text (UI.runTime ui)
      DearImGui.text (UI.burnTime ui)
      DearImGui.text (UI.topSpeed ui)

      DearImGui.newLine

      DearImGui.text (UI.position ui)
      DearImGui.text (UI.velocity ui)

    Dear.window_ (UI.boards ui) do
      DearImGui.text (UI.playerLines ui)
      DearImGui.separator
      DearImGui.text (UI.speedDump ui)
      DearImGui.separator
      DearImGui.text (UI.timeDump ui)
      DearImGui.separator
      DearImGui.text (UI.burnDump ui)

    Dear.window_ (UI.items ui) do
      DearImGui.withStyleColor DearImGui.ImGuiCol_PlotHistogram (pure @IO $ DearImGui.ImVec4 0.8 0 0 1) $
        DearImGui.progressBar
          (fst $ UI.boostItem ui)
          (Just . snd $ UI.boostItem ui)
      DearImGui.isItemHovered >>= \hovered ->
        when hovered $ DearImGui.withTooltip do
          DearImGui.text "Item: Boost"
          DearImGui.sameLine
          DearImGui.textDisabled "(W)"
          DearImGui.text $ Text.unlines
            [ "Activate to gain engine power."
            , "Maximum charges: 3"
            ]

      DearImGui.withStyleColor DearImGui.ImGuiCol_PlotHistogram (pure @IO $ DearImGui.ImVec4 0.8 0.8 0.8 1) $
        DearImGui.progressBar
          (fst $ UI.shedItem ui)
          (Just . snd $ UI.shedItem ui)
      DearImGui.isItemHovered >>= \hovered ->
        when hovered $ DearImGui.withTooltip do
          DearImGui.text "Item: Shed"
          DearImGui.sameLine
          DearImGui.textDisabled "(S)"
          DearImGui.text $ Text.unlines
            [ "Activate to quickly shed velocity."
            , "Maximum charges: 2"
            ]

      DearImGui.withStyleColor DearImGui.ImGuiCol_PlotHistogram (pure @IO $ DearImGui.ImVec4 0 0.8 0.8 1) $
        DearImGui.progressBar
          (fst $ UI.jumpItem ui)
          (Just . snd $ UI.jumpItem ui)
      DearImGui.isItemHovered >>= \hovered ->
        when hovered $ DearImGui.withTooltip do
          DearImGui.text "Item: Jump"
          DearImGui.sameLine
          DearImGui.textDisabled "(X)"
          DearImGui.text $ Text.unlines
            [ "Activate to warp forward without colliding."
            , "Maximum charges: 2"
            ]

    unless (Text.null $ UI.debugDump ui) $
      Dear.window_ (UI.debug ui) do
        DearImGui.text (UI.debugDump ui)

    when (isJust . Dear.windowCollapsed $ UI.raceEnd ui) $
      Dear.window_ (UI.raceEnd ui) do
        maxWidth <- DearImGui.getWindowWidth
        DearImGui.setNextItemWidth (maxWidth - 16.0)
        void $! DearImGui.inputTextWithHint
          "##playerName"
          "Your name"
          (UI.playerNameRef uiP)
          32

        DearImGui.newLine

        Dear.button_ (UI.nuke ui) $
          Dear.signal_ Event.Restart
        DearImGui.isItemHovered >>= \hovered ->
          when hovered $ DearImGui.withTooltip do
            DearImGui.text "Start again"
            DearImGui.sameLine
            DearImGui.textDisabled "(Esc)"
            DearImGui.text $ Text.unlines
              [ "Your score will be uploaded."
              , "Set your name for scoreboards."
              ]
