module Stage.Main.Render.Scene
  ( prepareCasters
  , prepareScene
  ) where

import RIO.Local

import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Resource.Model (Vertex3d)

import Global.Resource.Assets qualified as Assets
import Global.Resource.Model qualified as Model
import Global.Render (StageFrameRIO, Pipelines(..))
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.World qualified as World

type DrawM = StageFrameRIO FrameResources RunState

type DrawCasters dsl m = Vk.CommandBuffer -> Bound dsl (Vertex3d ()) Transform m ()

type DrawData dsl m =
  ( Vk.CommandBuffer -> Bound dsl Void Void m ()
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  )

prepareCasters
  :: ( -- Compatible '[Sun] dsl
     )
  => FrameResources
  -> DrawM (DrawCasters dsl DrawM)
prepareCasters FrameResources{frWorld} = do
  Model.Collection{..} <- gets $ Assets.aModels . rsAssets
  World.WorldBuffers{..} <- World.readObserved frWorld

  let
    drawCasters cb =
      Draw.indexedPos cb icosphere4 bodies

  pure drawCasters

prepareScene
  :: ( Compatible '[Scene] dsl
     )
  => Pipelines
  -> FrameResources
  -> DrawM (DrawData dsl DrawM)
prepareScene Pipelines{..} FrameResources{..} = do
  Model.Collection{..} <- gets $ Assets.aModels . rsAssets
  World.WorldBuffers{..} <- World.readObserved frWorld

  let
    drawOpaque cb = do
      Set0.withBoundSet0 frScene (Basic.pLitColored pBasic) cb do
        Graphics.bind cb (Basic.pLitColored pBasic) do
          -- TODO: use model from bodies
          Draw.indexed cb icosphere4 bodies
          Draw.indexed cb (craftRacer ^. _3) racers

    drawBlended cb = do
      Graphics.bind cb (Basic.pWireframe pBasic) do
        Draw.indexed cb debugWires zeroTransform

  pure (drawOpaque, drawBlended)
