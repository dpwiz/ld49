{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  ) where

import RIO.Local

import Engine.Events qualified as Events
import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..))
import Render.ImGui qualified as ImGui
import Resource.Region qualified as Region

import Global.Render qualified as Render
import Global.Resource.Assets (Assets)
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.Sink (handleEvent)
import Stage.Main.Render qualified as Render
import Stage.Main.Resource qualified as Resource
import Stage.Main.Types (RunState(..), Stage)

stackStage :: Assets -> StackStage
stackStage = StackStage . stage

stage :: Assets -> Stage
stage assets =
  Stage.assemble
    "Main"
    Render.component
    (Resource.component assets)
    (Just scene)
  where
    scene = Stage.Scene
      { scBeforeLoop = beforeLoop
      , scUpdateBuffers = Render.updateBuffers
      , scRecordCommands = Render.recordCommands
      }

    beforeLoop = do
      sink <- Region.local $ Events.spawn
        (handleEvent $ stackStage assets)
        [ Key.callback
        -- , CursorPos.callback cursorWindow
        -- , MouseButton.callback cursorCentered MouseButton.clickHandler
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      logDebug "Initializing ImGui"
      ImGui.allocateLoop True
