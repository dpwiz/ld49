{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Set0
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.Basic qualified as Basic
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass

import Global.Render (StageFrameRIO, RenderPasses(..), Pipelines(..))
import Stage.Main.Render.Scene (prepareCasters, prepareScene)
import Stage.Main.Render.UI (imguiDrawData)
import Stage.Main.Types (FrameResources(..), RunState(..))
-- import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World

updateBuffers
  :: RunState
  -> FrameResources
  -> StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi

  World.observe rsWorldSim frWorld

  -- UI.observe rsUI frUI

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> StageFrameRIO FrameResources RunState ()
recordCommands cb fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let
    RenderPasses{..} = fRenderpass
    Pipelines{..} = fPipelines

  sceneDrawCasters <- prepareCasters fr
  (sceneDrawOpaque, sceneDrawBlended) <- prepareScene fPipelines fr
  dear <- imguiDrawData

  updateShadowP <- gets rsUpdateShadowP
  updateShadow <- gets rsUpdateShadow
  Worker.observeIO_ updateShadowP updateShadow \() () -> do
    Worker.Versioned{vVersion} <- readTVarIO (Worker.getOutput updateShadowP)
    logDebug $ "Updating shadowmap for version " <> displayShow vVersion
    let shadowLayout = Graphics.pLayout $ Basic.pShadowCast pBasic
    ShadowPass.usePass (Basic.rpShadowPass rpBasic) imageIndex cb do
      let shadowArea = ShadowPass.smRenderArea (Basic.rpShadowPass rpBasic)
      Swapchain.setDynamic cb shadowArea shadowArea
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
        Graphics.bind cb (Basic.pShadowCast pBasic) $
          sceneDrawCasters cb

  ForwardMsaa.usePass (Basic.rpForwardMsaa rpBasic) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (Basic.pWireframe pBasic) cb do
      sceneDrawOpaque cb
      sceneDrawBlended cb

    ImGui.draw dear cb
