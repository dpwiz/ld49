{-# OPTIONS -Wwarn #-}

module Stage.Main.UI
  ( Input(..)
  , playerNameRef
  , Stuff(..)
  , Process
  , spawn
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Data.StateVar (StateVar)
import DearImGui (ImVec2(..))
import DearImGui qualified
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Formatting ((%))
import Formatting qualified as F
import Geomancy.Layout.Box (Box(..))
import Highscores qualified as Score
import RIO.Text qualified as Text

import Global.Highscores qualified as Highscores
import Global.Resource.Assets (Assets)
import Stage.Main.World qualified as World
import Stage.Main.World.Racer qualified as Racer -- TODO: move to Items
import Utils.Dear qualified as Dear

data Input = Input
  { _playerPosition :: Vec3
  , _playerVelocity :: Vec3

  , _boostItem :: Either Float Float
  , _shedItem  :: Either Float Float
  , _jumpItem  :: Either Float Float

  , _crashAlert :: Maybe Float
  , _crashed    :: Bool

  , _goalDistance :: Float
  , _runTime      :: Float
  , _burnTime     :: Float

  , _topScore :: Int
  , _topSpeed :: Float

  , _player :: Highscores.Player
  , _boards :: Maybe Highscores.Boards

  , _debugDump :: Text
  } deriving (Show, Generic)

playerNameRef :: Process -> StateVar Text
playerNameRef = Worker.stateVarMap getter setter
  where
    getter =
      Highscores.playerName . _player

    setter newName input = input
      { _player = (_player input)
          { Highscores.playerName =
              newName
          }
      }

data Stuff = Stuff
  { status   :: Dear.Window
  , position :: Text
  , velocity :: Text

  , items     :: Dear.Window
  , boostItem :: (Float, Text)
  , shedItem  :: (Float, Text)
  , jumpItem  :: (Float, Text)

  , goalDistance :: Text
  , crashAlert   :: Text

  , runTime  :: Text
  , burnTime :: Text
  -- , topScore :: Text
  , topSpeed :: Text

  , nuke     :: Dear.Button

  , boards :: Dear.Window
  , playerLines :: Text
  , speedDump :: Text
  , timeDump :: Text
  , burnDump :: Text

  , raceEnd :: Dear.Window

  , debug :: Dear.Window
  , debugDump :: Text
  }

type Process = Worker.Cell Input Stuff

spawn
  :: Assets
  -> Worker.Merge Box
  -> ResourceT (Engine.StageRIO env) Process
spawn assets boxP = do
  player <- Highscores.getPlayer ""
  boards <- Highscores.fetch (Highscores.playerName player)

  inputVar <- Worker.newVar Input
    { _playerPosition = 0
    , _playerVelocity = 0

    , _boostItem = Right 0
    , _shedItem  = Right 0
    , _jumpItem  = Right 0

    , _crashAlert = Nothing
    , _crashed    = False

    , _topScore = 0
    , _topSpeed = 0

    , _goalDistance = recip 0
    , _runTime = 0
    , _burnTime = 0

    , _player = player
    , _boards = boards

    , _debugDump = ""
    }
  process <- Worker.spawnMerge2 (mkUI assets) boxP inputVar
  pure (inputVar, process)

mkUI :: Assets -> Box -> Input -> Stuff
mkUI _assets Box{size} Input{..} = Stuff{..}
  where
    status = Dear.Window
      { windowLabel =
          case _crashed of
            False ->
              "Flying"
            True | arrived ->
              "Arrived"
            True ->
              "Crashed"
      , windowFlags =
          staticNoCollapse
      , windowFullscreen =
          False
      , windowPos =
          ImVec2 25 25
      , windowPivot =
          ImVec2 0 0
      , windowSize =
          Just $ ImVec2 350 175
      , windowAlpha =
          Nothing
      , windowCollapsed =
          Nothing
      }

    arrived =
      _goalDistance < World.goalRadius

    items = Dear.Window
      { windowLabel =
          "Items"
      , windowFlags =
          staticNoCollapse
      , windowFullscreen =
          False
      , windowPos =
          ImVec2 (maxWidth * 0.5) 25
      , windowPivot =
          ImVec2 0.5 0
      , windowSize =
          Just $ ImVec2 150 100
      , windowAlpha =
          Just 0.25
      , windowCollapsed =
          Nothing
      }

    boostItem =
      case _boostItem of
        Left timer ->
          ( 1.0 - timer / Racer.maxBoostTimer
          , "Boosting"
          )
        Right charge ->
          ( min 1.0 charge
          , F.sformat
              ("Boost charges: " % F.flooredTo @Int F.int)
              charge
          )

    shedItem =
      case _shedItem of
        Left timer ->
          ( 1.0 - timer / Racer.maxBoostTimer
          , "Shedding"
          )
        Right charge ->
          ( min 1.0 charge
          , F.sformat
              ("Shed charges: " % F.flooredTo @Int F.int)
              charge
          )

    jumpItem =
      case _jumpItem of
        Left timer ->
          ( 1.0 - timer / Racer.maxJumpTimer
          , "Jumping"
          )
        Right charge ->
          ( min 1.0 charge
          , F.sformat
              ("Jump charges: " % F.flooredTo @Int F.int)
              charge
          )

    goalDistance =
      F.sformat
        ("Distance to goal: " % F.fixed 1)
        _goalDistance

    crashAlert =
      case _crashAlert of
        Just seconds | not _crashed ->
          F.sformat
            ("Crash alert: " % F.fixed 1)
            seconds
        _ ->
          ""

    runTime =
      F.sformat
        ("Run time: " % F.fixed 1)
        _runTime

    burnTime =
      F.sformat
        ("Burn time: " % F.fixed 1)
        _burnTime

    -- topScore =
    --   F.sformat
    --     ("Score: " % F.int)
    --     _topScore

    topSpeed =
      F.sformat
        ("Top speed: " % F.fixed 1)
        _topSpeed

    position =
      withVec3 _playerPosition \x y _z ->
        F.sformat
          ("Position: (" % F.fixed 1 % "," % F.fixed 1 % ")")
          x
          y

    velocity =
      withVec3 _playerVelocity \x y _z ->
        F.sformat
          ("Velocity: (" % F.fixed 1 % "," % F.fixed 1 % ")")
          x
          y

    nuke =
      Dear.Button
        if _crashed then
          "Reassemble"
        else
          "Submit score"

    boards = Dear.Window
      { windowLabel =
          "High scores"
      , windowFlags =
          staticNoCollapse
      , windowFullscreen =
          False
      , windowPos =
          ImVec2 25 225
      , windowPivot =
          ImVec2 0 0
      , windowSize =
          Just $ ImVec2 350 (maxHeight - 250)
      , windowAlpha =
          Just 0.25
      , windowCollapsed =
          if _crashed then
            Just False
          else
            Nothing
      }

    playerLines = case _boards of
      Nothing ->
        ""
      Just Highscores.Boards{..} ->
        Text.unlines $ catMaybes
          [ personalScore "Speed" speeds
          , personalScore "Time" times
          , personalScore "Burn" burns
          ]

    speedDump = case fmap Highscores.speeds _boards of
      Nothing ->
        ""
      Just Score.ScoreBoard{topScores} ->
        Text.unlines $
          "Speed records" :
          map globalScore (take 25 topScores)

    timeDump = case fmap Highscores.times _boards of
      Nothing ->
        ""
      Just Score.ScoreBoard{topScores} ->
        Text.unlines $
          "Time records" :
          map globalScore (take 25 topScores)

    burnDump = case fmap Highscores.burns _boards of
      Nothing ->
        ""
      Just Score.ScoreBoard{topScores} ->
        Text.unlines $
          "Burn records" :
          map globalScore (take 25 topScores)

    raceEnd = Dear.Window
      { windowLabel =
          if arrived then
            "Arrived!"
          else
            "Crashed!"
      , windowFlags =
          staticNoCollapse
      , windowFullscreen =
          False
      , windowPos =
          ImVec2 (maxWidth * 0.5) (maxHeight * 0.8)
      , windowPivot =
          ImVec2 0.5 0.5
      , windowSize =
          Just $ ImVec2 200 100
      , windowAlpha =
          Nothing
      , windowCollapsed =
          if _crashed then
            Just False
          else
            Nothing
      }

    debug = Dear.Window
      { windowLabel =
          "##DebugDump"
      , windowFlags =
          DearImGui.fullscreenFlags
      , windowFullscreen =
          False
      , windowPos =
          ImVec2 (25 + 400) (maxHeight - 25)
      , windowPivot =
          ImVec2 0 1
      , windowSize =
          Just $ ImVec2 (maxWidth - 50 - 400) 100
      , windowAlpha =
          Nothing
      , windowCollapsed =
          Nothing
      }

    debugDump =
      _debugDump

    (maxWidth, maxHeight) = withVec2 size (,)

    staticNoCollapse =
      Dear.windowFlagsStatic .|.
      Dear.windowFlagsNoCollapse

personalScore :: Text -> Score.ScoreBoard -> Maybe Text
personalScore label =
  fmap render . Score.currentPlayerScore
  where
    render Score.Score{..} =
      F.sformat
        (F.right 10 ' ' % ": " % F.int % " (#" % F.int % ")")
        label
        score
        placement

globalScore :: Score.Score -> Text
globalScore Score.Score{..} =
  F.sformat
    (F.left 3 ' ' % " | " % F.left 32 ' ' % " | " % F.left 6 ' ')
    placement
    ( if Text.null playerName then
        "(unknown)"
      else
        playerName
    )
    score
