module Stage.Main.Scene where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Render.DescSets.Set0 (Scene(..), emptyScene)
import Render.DescSets.Sun (Sun(..), SunInput(..), initialSunInput, mkSun)
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource (MonadResource)

type CameraProcess = Worker.Timed CameraControls Camera.View

data CameraControls = CameraControls
  { ccAzimuth  :: Float
  , ccAscent   :: Float
  , ccDistance :: Float
  , ccPan      :: Vec3
  , ccTrack    :: CameraTrack
  }

data CameraTrack
  = CameraTrackNothing
  | CameraTrackFixed Vec3
  | CameraTrackLerp Vec3 Float
  deriving (Eq, Ord, Show)

data CameraEvent
  = CameraAzimuth Float
  | CameraAscent Float
  | CameraDistance Float
  | CameraPan Vec3

spawnCamera :: (MonadResource m, MonadUnliftIO m) => m CameraProcess
spawnCamera =
  Worker.spawnTimed
    False
    (Left interval)
    mkState
    (mkView dt)
    initialConfig
  where
    interval = 10_000
    dt = 1 / fromIntegral interval

    mkState _config =
      pure
        ( Camera.mkViewOrbital_ initialState
        , initialState
        )

    -- zoomSpeed = CLIP_FAR

initialState :: Camera.ViewOrbitalInput
initialState = Camera.initialOrbitalInput
  { Camera.orbitAzimuth  = τ/2
  , Camera.orbitAscent   = 0
  , Camera.orbitDistance = 50
  }

initialConfig :: CameraControls
initialConfig = CameraControls
  { ccAzimuth  = 0
  , ccAscent   = 0
  , ccDistance = 0
  , ccPan      = 0
  , ccTrack    = CameraTrackNothing
  }

mkView
  :: Monad m
  => Float
  -> Camera.ViewOrbitalInput
  -> CameraControls
  -> m (Maybe Camera.View, Camera.ViewOrbitalInput)
mkView dt Camera.ViewOrbitalInput{..} CameraControls{..} = do
  let
    (doPan, newTarget) =
      case ccTrack of
        CameraTrackNothing ->
          if ccPan /= 0 then
            (True, orbitTarget + ccPan Vec3.^* dt)
          else
            (False, orbitTarget)
        CameraTrackFixed trackTarget ->
          ( True -- orbitTarget /= trackTarget
          , trackTarget
          )
        CameraTrackLerp trackTarget alpha ->
          ( True
          , Vec3.lerp alpha orbitTarget trackTarget
          )

    adjust = or
      [ ccAzimuth /= 0
      , ccAscent /= 0
      , ccDistance /= 0
      , doPan
      ]
    state' = Camera.ViewOrbitalInput
      { orbitAzimuth  = orbitAzimuth + ccAzimuth * dt
      , orbitAscent   = trimAscent $ orbitAscent + ccAscent * dt
      , orbitDistance = trimDistance $ orbitDistance + ccDistance * dt
      , orbitTarget   = newTarget
      , ..
      }
  pure
    ( if adjust then
        Just $ Camera.mkViewOrbital_ state'
      else
        Nothing
    , state'
    )
  where
    trimAscent = trim (-τ/4 + eta) (τ/4 - eta)
      where
        eta = 1/64

    trimDistance =
      trim
        1 -- (6371 * World.SCALE + 2) -- XXX: planet radius + clip near
        (CLIP_FAR * 0.5) -- XXX: half clip far


-- Engine hardcode
pattern CLIP_FAR :: (Eq a, Num a) => a
pattern CLIP_FAR = 16384

type Process = Worker.Merge Scene

mkScene :: Camera.Projection 'Camera.Perspective -> Camera.View -> Scene
mkScene Camera.Projection{..} Camera.View{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = projectionInverse

    , sceneView          = viewTransform
    , sceneInvView       = viewTransformInv
    , sceneViewPos       = viewPosition
    , sceneViewDir       = viewDirection

    , sceneEnvCube       = -1 -- CubeMap.milkyway CubeMap.indices
    , sceneNumLights     = fromIntegral $ Storable.length staticLights
    }

mkSceneUi :: Camera.Projection 'Camera.Orthographic -> Scene
mkSceneUi Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = projectionInverse
    }

staticLights :: Storable.Vector Sun
staticLights =
  Storable.fromList
    [ mkLight (-τ*1/3) (τ/5) 2.0
    , mkLight 0        (τ/6) 2.0
    ]

mkLight :: Float -> Float -> Vec3 -> Sun
mkLight azimuth inclination color =
  snd $ mkSun initialSunInput
    { siAzimuth     = azimuth
    , siInclination = inclination
    , siColor       = Vec4.fromVec3 color 1.0
    }
