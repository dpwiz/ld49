{-# OPTIONS -Wwarn #-}

module Utils.Dear where

import RIO.Local

import DearImGui.Internal.Text qualified as Text
import DearImGui.Raw qualified as Raw
import Engine.Events (Sink(..))
import Foreign (nullPtr, with)
import Foreign.C (CFloat(..))
-- import Render.ImGui qualified as ImGui

-- import Global.Render (StageFrameRIO)
-- import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.Event.Type as Event
import Utils.Sink (MonadSink(..))

-- TODO: HasEvents rs
signalAction
  :: MonadSink Event rs m
  => m Event
  -> m ()
signalAction action = do
  events <- getSink
  for_ events \(Sink signal) -> do
    event <- action
    sendSinkSignal $ signal event

signal_
  :: MonadSink Event rs m
  => Event
  -> m ()
signal_ = signalAction . pure

data Window = Window
  { windowLabel      :: Text
  , windowFlags      :: Raw.ImGuiWindowFlags
  , windowFullscreen :: Bool
  , windowPos        :: Raw.ImVec2
  , windowPivot      :: Raw.ImVec2
  , windowSize       :: Maybe Raw.ImVec2
  , windowAlpha      :: Maybe Float
  , windowCollapsed  :: Maybe Bool
  }

windowFlagsNoCollapse :: Raw.ImGuiWindowFlags
windowFlagsNoCollapse = Raw.ImGuiWindowFlags_NoCollapse

windowFlagsStatic :: Raw.ImGuiWindowFlags
windowFlagsStatic =
  Raw.ImGuiWindowFlags_NoResize .|.
  Raw.ImGuiWindowFlags_NoBringToFrontOnFocus .|.
  Raw.ImGuiWindowFlags_NoFocusOnAppearing

data Button = Button
  { buttonLabel  :: Text
  -- , buttonAction :: m ()
  }

button_ :: MonadUnliftIO m => Button -> m () -> m ()
button_ Button{..} buttonAction = do
  action <- toIO buttonAction
  liftIO $ Text.withCString buttonLabel \labelPtr ->
    Raw.button labelPtr >>= (`when` action)

window_ :: MonadUnliftIO m => Window -> m () -> m ()
window_ Window{..} action = bracket open close (`when` action)
  where
    open = liftIO do
      if windowFullscreen then
        Raw.setNextWindowFullscreen
      else do
        with windowPos \posPtr ->
          with windowPivot \pivotPtr ->
            Raw.setNextWindowPos posPtr Raw.ImGuiCond_Always (Just pivotPtr)

        for_ windowSize \size ->
          with size \sizePtr ->
            Raw.setNextWindowSize sizePtr Raw.ImGuiCond_Always

        for_ windowAlpha $
          Raw.setNextWindowBgAlpha . CFloat

        for_ windowCollapsed \collapsed ->
          Raw.setNextWindowCollapsed (bool 0 1 collapsed) Raw.ImGuiCond_Always

      Text.withCString windowLabel \labelPtr ->
        Raw.begin labelPtr (Just nullPtr) (Just windowFlags)

    close =
      liftIO . const Raw.end
